<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<%@ include file="/WEB-INF/layouts/base_style.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title> ${config.webTitle} </title>
    <meta charset="utf-8">
</head>
<body>
<jsp:include page="../index/header.jsp"/>
<div id="nav-shadow"></div>
<div id="content">

    <div class="container">
        <div id="main">
            <%--文章显示每个div--%>
            <c:forEach items="${vo.articles }" var="item" varStatus="vs">
                <div class="entry">

                    <div class="entry-header">

                        <h2 class="title"><a href="${ctx }/article/456${item.article.recordId}.html">${item.article.title}</a></h2>

                        <p class="meta">编辑于 <a href="#">${item.lastDate}</a> by <a href="#">${item.authorName}</a></p>

                        <a href="${ctx }/article/456${item.article.recordId}.html" class="button">Read More...</a>

                    </div><!-- end .entry-header -->

                    <div class="entry-content">

                        <a href="${ctx }/article/456${item.article.recordId}.html"><img src="${config.adminUrl }titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/001.png'" width="240" height="140" alt="" class="entry-image" /></a>

                        <p><itfarm:StringCut length="350" strValue="${item.article.content }"></itfarm:StringCut></p>

                        <hr />

                        <ul class="entry-links">
                            <li><a href="#">Send to a Friend</a> <span class="separator">|</span></li>
                            <li><a href="#">Share this Post</a> <span class="separator">|</span></li>
                            <li><a href="#">41 Comments</a> <span class="separator">|</span></li>
                            <li><a href="#">0 Tweets</a></li>
                        </ul>

                    </div><!-- end .entry-content -->

                </div><!-- end .entry -->
            </c:forEach>
            <%--分页--%>
            <itfarm:PageBar pageUrl="/article/index.do" pageAttrKey="page"></itfarm:PageBar>

        </div><!-- end #main -->
        <%--右侧竖栏--%>
        <jsp:include page="../index/right_menu.jsp"/>
        <div class="clear"></div>

    </div><!-- end .container -->


</div><!-- end #content -->
<jsp:include page="../index/right.jsp"></jsp:include>
<%--底部--%>
<jsp:include page="../index/footer.jsp"/>

</body>
</html>