<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<%@ include file="/WEB-INF/layouts/base_style.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title> ${config.webTitle} </title>
    <meta charset="utf-8">
    <%--评论框--%>
    <link rel="stylesheet" href="${ctx}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/sinaFaceAndEffec.css"/>
    <script type="text/javascript">
        $(function () {
            var content_height = $('#article_content').height();
            if (content_height > 1000) {
                $('#article_content').css('height','1000px');
                $('#read_more_btn').click(function () {
                    $('#article_content').css('height','auto');
                    $('#read_more_btn').css('display', 'none');
                });
            } else {
                $('#article_content').css('height','auto');
                $('#read_more_btn').css('display', 'none');
            }
        });
        
    </script>
</head>
<body>
<jsp:include page="../index/header.jsp"/>
<div id="main_content" class="container">

    <div class="col-md-8">
        <div class="col-md-12 panel panel-default">

            <div class="row" style="padding: 5px;">
                <div style="width: 100%; height: 80px; text-align: center">
                    <h2 class="title">${item.article.title }&nbsp;<a
                            href="${ctx }/article/456${item.article.recordId}t.html"
                            style="font-size: 10px;color: #8BBF5D;">(宽屏浏览)</a></h2>
                    <p id="" style="font-size: 8px; color: #A9A9A9;">
                        <span class="glyphicon glyphicon-time"></span>&nbsp;${item.lastDate }&nbsp;&nbsp;
                        <span class="glyphicon glyphicon-comment"></span>&nbsp;评论(<strong>${item.commentCount}</strong>)&nbsp;&nbsp;
                        <span class="glyphicon glyphicon-eye-open"></span>&nbsp;阅读(<strong>${item.article.pageView}</strong>)&nbsp;&nbsp;
                        <span class="glyphicon glyphicon-folder-open"></span>&nbsp;<a>${item.categoryName}</a>&nbsp;&nbsp;
                    </p>
                </div>
            </div><!-- end .entry-header -->
            <hr/>
            <div id="article_content" style="overflow: hidden;">
                ${item.article.content }
            </div><!-- end .entry-content -->
            <div class="read_more">
                <button type="button" id="read_more_btn" class="btn btn-primary">查看全部</button>
            </div><!
            <hr/>
            <div style="text-align: center; width: 100%; height: 40px;">
                <!-- JiaThis Button BEGIN -->
                <div class="jiathis_style"><span class="jiathis_txt">分享到：</span>
                    <a class="jiathis_button_qzone"></a>
                    <a class="jiathis_button_tsina"></a>
                    <a class="jiathis_button_tqq"></a>
                    <a class="jiathis_button_weixin"></a>
                    <a class="jiathis_button_renren"></a>
                    <a class="jiathis_button_cqq"></a>
                    <a class="jiathis_button_email"></a>
                    <a class="jiathis_button_fb"></a>
                    <a class="jiathis_button_twitter"></a>
                    <a class="jiathis_button_tianya"></a>
                    <a href="http://www.jiathis.com/share"
                       class="jiathis jiathis_txt jiathis_separator jtico jtico_jiathis" target="_blank"></a>
                    <a class="jiathis_counter_style"></a>
                </div>
            </div>
            <br>
            <script type="text/javascript">
                var jiathis_config = {
                    summary: "",
                    shortUrl: false,
                    hideMore: false
                }
            </script>
            <script type="text/javascript" src="http://v3.jiathis.com/code/jia.js" charset="utf-8"></script>
            <!-- JiaThis Button END -->
            <div class="box-footer">
                <div class="align-left">
                    <c:if test="${item.last != null }">
                        <strong>上一篇:</strong>&nbsp;<a href="${ctx }/article/456${item.last.recordId}f.html">${item.last.title }</a>
                    </c:if>
                </div><!-- end .align-left -->
                <div class="align-right">
                    <c:if test="${item.next != null }">
                        <strong>下一篇:</strong>&nbsp;<a href="${ctx }/article/456${item.next.recordId}f.html">${item.next.title }</a>
                        </ul>
                    </c:if>
                </div><!-- end .align-right -->
            </div><!-- end .box-footer -->
            <hr>
            <div class="box-footer">
                <%--分类--%>
                <span><strong>Category:</strong>&nbsp;<a href="#">${item.categoryName}</a></span>&nbsp;&nbsp;
                <span><strong>Tags:</strong>&nbsp;<a href="#">${item.article.keyword }</a></span>
            </div><!-- end .box-footer -->
        </div><!-- end .entry -->
        <div class="col-md-12 panel panel-default">
            <div id="content" style="width: auto; height: auto;margin-top:40px">
                <div class="wrap">
                    <div class="comment">
                        <div class="head-face">
                            <img src="${ctx}/images/logo.png"/>
                            <p>我是鸟</p>
                        </div>
                        <div class="content">
                            <div class="cont-box">
                                <textarea id="comment_area" class="text" placeholder="说点什么吧..."></textarea>
                            </div>
                            <div class="tools-box">
                                <div class="operator-box-btn">
                                    <span class="face-icon">☺</span>
                                    <span class="img-icon">▧</span>
                                    <span class="is_visitor fr"><input id="is_vi" type="checkbox">游客身份</span>
                                </div>
                                <div class="submit-btn"><input type="button" onClick="out()" value="提交评论"/></div>
                            </div>
                        </div>
                    </div>
                    <div id="info-show">
                        <%--显示评论--%>
                        <ul id="main_ul">
                            <c:forEach items="${commentVOs}" var="item">
                            <li>
                                <div class="head-face">
                                    <img src="${ctx}/images/logo.png">
                                </div>
                                <div class="reply-cont">
                                    <p class="username">${item.comment.nickName}</p>
                                    <p class="comment-body">
                                        ${item.comment.content}
                                    </p>
                                    <p class="comment-footer"><fmt:formatDate value="${item.comment.createTime}" pattern="yyyy年MM月dd日 HH:mm:ss"/>　回复　点赞(${item.comment.likeCount})</p>
                                </div>
                                <%--二次回复--%>
                                <c:if test="${fn:length(item.child) > 0}">
                                <div>
                                    <ul>
                                        <c:forEach items="${item.child}" var="i">
                                        <li style="text-indent: 50px;">
                                            <div class="head-face">
                                                <img src="${ctx}/images/logo.png">
                                            </div>
                                            <div class="reply-cont">
                                                <p class="username">${i.comment.nickName}</p>
                                                <p class="comment-body">
                                                        ${i.comment.content}
                                                </p>
                                                <p class="comment-footer"><fmt:formatDate value="${i.comment.createTime}" pattern="yyyy年MM月dd日 HH:mm:ss"/>　回复　点赞(${i.comment.likeCount})</p></div>
                                            <c:if test="${fn:length(i.child) > 0}">
                                                <div>
                                                    <ul>
                                                        <c:forEach items="${i.child}" var="ii">
                                                            <li style="text-indent: 50px;">
                                                                <div class="head-face">
                                                                    <img src="${ctx}/images/logo.png">
                                                                </div>
                                                                <div class="reply-cont">
                                                                    <p class="username">${ii.comment.nickName}</p>
                                                                    <p class="comment-body">
                                                                            ${ii.comment.content}
                                                                    </p>
                                                                    <p class="comment-footer"><fmt:formatDate value="${ii.comment.createTime}" pattern="yyyy年MM月dd日 HH:mm:ss"/>　回复　点赞(${ii.comment.likeCount})</p></div>
                                                            </li>
                                                        </c:forEach>
                                                    </ul>
                                                </div>
                                            </c:if>
                                        </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                                </c:if>
                            </li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="comment_dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">温馨提示</h4>
                    </div>
                    <div class="modal-body">您尚未登录哦!<a href="#">快速登录</a></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <script type="text/javascript" src="${ctx}/js/main.js"></script>
        <script type="text/javascript" src="${ctx}/js/sinaFaceAndEffec.js"></script>
        <script type="text/javascript">
            // 绑定表情
            $('.face-icon').SinaEmotion($('.text'));
            // 测试本地解析
            function out() {
                var is_v = $('#is_vi').is(':checked');
                if (!is_v) {
                    if (${user == null}) {
                        $('#comment_dialog').modal({
                            keyboard: true
                        })
                        return;
                    }
                }
                var inputText = $('.text').val();
                if (inputText == "") {
                    alert("请输入内容!");
                    return;
                }
                // 发送评论请求
                $.post("${ctx}/comment/save.do",
                        {
                            content:AnalyticEmotion(inputText),
                            type : 0,
                            parentId : ${item.article.recordId},
                            nickName : ${user == null} ? "游客" : "",
                            articleTitle : '${item.article.title}'
                        },
                        function(result){
                            if (result.success) {
                                $('#info-show #main_ul').prepend(reply(AnalyticEmotion(inputText)));
                                $('#comment_area').val('');
                            } else {
                                alert("系统故障，请重试!");
                            }
                        });
            }

            var html;
            function reply(content) {
                var mydate = new Date();
                var time = mydate.getFullYear() + "年" + (mydate.getMonth()+1) + "月" + mydate.getDate() + "日" +
                        mydate.getHours() + ":" + mydate.getMinutes()+":" + mydate.getSeconds();
                html = '<li>';
                html += '<div class="head-face">';
                html += '<img src="${ctx}/images/logo.png" / >';
                html += '</div>';
                html += '<div class="reply-cont">';
                html += '<p class="username">游客</p>';
                html += '<p class="comment-body">' + content + '</p>';
                html += '<p class="comment-footer">'+ time +'　<a href="#">回复</a>　点赞(0)</p>';
                html += '</div>';
                html += '</li>';
                return html;
            }
        </script>
    </div>
    <jsp:include page="../index/right.jsp"></jsp:include>
</div>
<%--底部--%>
<jsp:include page="../index/footer.jsp"/>

</body>
</html>