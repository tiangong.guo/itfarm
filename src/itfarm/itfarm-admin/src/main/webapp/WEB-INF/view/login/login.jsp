<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>ITFARM后台管理中心登陆</title>
	<meta name="keywords" content="${systemConfig.adminTitle}">
	<meta name="content" content="${systemConfig.adminTitle}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <link href="${ctx}/css/login.css" rel="stylesheet" type="text/css">
</head>

<body class="login">

<div class="login_m">
    <div class="login_logo"><img src="${ctx}/images/logo.jpg" width="196" height="46"></div>
    <div class="login_boder">
        <div class="login_padding">
            <form action="${ctx}/login.do" method="post">
            <h2>用户名</h2>
            <label>
                <input type="text" name="username" id="username" class="txt_input txt_input2" onfocus="if (value ==&#39;Your name&#39;){value =&#39;&#39;}" onblur="if (value ==&#39;&#39;){value=&#39;Your name&#39;}" value="Your name">
            </label>
            <h2>密码</h2>
            <label>
                <input type="password" name="password" id="userpwd" class="txt_input" onfocus="if (value ==&#39;******&#39;){value =&#39;&#39;}" onblur="if (value ==&#39;&#39;){value=&#39;******&#39;}" value="******">
            </label>
            <p class="forgot"><a href="javascript:void(0);">忘记密码?</a></p>
            <p class="forgot" style="float: left; color: red;">${message}</p>
            <div class="rem_sub">
                <div class="rem_sub_l" style="margin-left: 50px;">
                    <input type="checkbox" name="checkbox" id="save_me">
                    <label for="save_me">记住</label>
                </div>
                <label>
                    <input type="submit" class="sub_button" name="button" id="button" value="登录" style="opacity: 0.7;">
                </label>
            </div>
            </form>
        </div>
    </div><!--login_boder end-->
</div><!--login_m end-->

<br />
<br />
</body>
</html>