<%--标签 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${ctx}/css/reset.css" type="text/css" media="screen" />
<link rel="stylesheet" href="${ctx}/css/index.css" type="text/css" media="screen" />

<!--[if ! lte IE 6]><!-->
<link rel="stylesheet" href="${ctx}/css/style.css" type="text/css" media="screen" />
<!--<![endif]-->

<!--[if gt IE 6]>
<link rel="stylesheet" href="${ctx}/css/ie.css" type="text/css" media="screen" />
<![endif]-->

<!--[if IE 7]>
<link rel="stylesheet" href="${ctx}/css/ie7.css" type="text/css" media="screen" />
<![endif]-->

<!--[if lte IE 6]>
<link rel="stylesheet" href="http://universal-ie6-css.googlecode.com/files/ie6.1.1.css" media="screen, projection">
<![endif]-->

<link rel="stylesheet" href="${ctx}/css/fancybox.css" type="text/css" media="screen" />
<script type="text/javascript">
    var basePath = "${staticPath }";
</script>
<!-- start scripts -->
<script src="${ctx}/js/jquery.min.js"></script>
<script src="${ctx}/js/jquery.cycle.all.min.js"></script>
<script src="${ctx}/js/jquery.easing.1.3.js"></script>
<script src="${ctx}/js/organictabs.jquery.js"></script>
<script src="${ctx}/js/jquery.fancybox-1.3.4.pack.js"></script>
<script src="${ctx}/js/css3-mediaqueries.js"></script>
<script src="${ctx}/js/custom.js"></script>
<!--[if IE]> <script src="${ctx}/js/selectivizr.js"></script> <![endif]-->
<!-- end scripts -->
